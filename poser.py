import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import sys

sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from Helpers.helpers import *

#from camera_calibrate import CalibrateCamera
from camera_pose import Rotate





class Poser:
    '''K = camera intrinsic matrix
    Rt = camera extrinsic matrix(s)
    R = camera orientation matrix, R
    r = camera orientation vector, rvec
    t = camera position vector, tvec'''

    def __init__(self, chessboard_dim=(6,9), chessboard_sq=1):
        self.chessboard_dim = chessboard_dim
        self.chessboard_sq = chessboard_sq
        self.n_sq = 3 * self.chessboard_sq - 1


        self.setObjectCorners() 
        


    def show(self, poses='all', pose_n=True, pose_xyz=False, pose_lines=False, figsize=(10,10)):
        self.generate_figure(figsize)
        self.draw_chessboard()

        if poses == 'all':
            poses = self.poses
        else:
            poses = {k:v for k,v in self.poses.items() if k in poses}

        for k,v in poses.items():
            if pose_n is False or len(poses.keys()) == 1:
                k = False
            r, t = v['r'], v['t']
            self.draw_position(t, pose_n=k, pose_xyz=pose_xyz, pose_lines=pose_lines)
            self.draw_orientation(r, t)

        #lim = np.abs(self.t).max()
        lim = max(np.abs(self.t).max(), max(self.chessboard_dim)) * 1.1
        self.ax.set_xlim(lim, -lim)
        self.ax.set_ylim(-lim, lim)
        self.ax.set_zlim(0, 2*lim)
        plt.show(block=False)


    def generate_figure(self, figsize):#, t):
        self.fig = plt.figure(figsize=figsize)
        self.ax = plt.axes(projection='3d')

        self.xcolor = 'blue'
        self.ycolor = 'lime'
        self.zcolor = 'red'

        # Label, color axes
        #plt.title(FILE)
        self.ax.set_xlabel('X (au)'); self.ax.set_ylabel('Y (au)'); self.ax.set_zlabel('Z (au)')
        self.ax.xaxis.label.set_color(self.xcolor); self.ax.tick_params(axis='x', colors=self.xcolor)
        self.ax.yaxis.label.set_color(self.ycolor); self.ax.tick_params(axis='y', colors=self.ycolor)
        self.ax.zaxis.label.set_color(self.zcolor); self.ax.tick_params(axis='z', colors=self.zcolor)

        # Set background from grey to white
        bgcolor = (1,1,1)
        self.ax.w_xaxis.set_pane_color(bgcolor)
        self.ax.w_yaxis.set_pane_color(bgcolor)
        self.ax.w_zaxis.set_pane_color(bgcolor)

        plt.tight_layout()


    def setObjectCorners(self):
        w, h = self.chessboard_dim
        x = np.tile(range(0, w*self.chessboard_sq, self.chessboard_sq), h)
        y = np.repeat(range(0, h*self.chessboard_sq, self.chessboard_sq), w)
        z = np.zeros(w*h)
        obj_corners = np.vstack([x, y, z]).T
        obj_corners = obj_corners.reshape(1,-1,3).astype(np.float32)
        self.obj_corners = obj_corners


    def draw_chessboard(self):
        '''Draw chessboard at origin'''
        c = 0
        z = [0,0,0,0]
        w, h = self.chessboard_dim
        for i in range(-1, w):
            for j in range(-1, h):
                x = np.array([i, i, i+1, i+1]) * self.chessboard_sq
                y = np.array([j, j+1, j+1, j]) * self.chessboard_sq
                points = [list(zip(x,y,z))]
                fc = 'w' if c%2 else 'k'
                self.ax.add_collection3d(Poly3DCollection(points, facecolor=fc, alpha=0.5))
                c += 1
            if h%2: c += 1
    
        # Draw chessboard axes
        lw = 2
        self.ax.quiver(0,0,0, self.n_sq,0,0, color=self.xcolor, lw=lw)
        self.ax.quiver(0,0,0, 0,self.n_sq,0, color=self.ycolor, lw=lw)
        self.ax.quiver(0,0,0, 0,0,self.n_sq, color=self.zcolor, lw=lw)


    def draw_position(self, t, pose_n=True, pose_xyz=False, pose_lines=False):
        if pose_lines:
            # Draw camera position, line
            #self.ax.plot(*t, 'x', c=c)#facecolors='none', edgecolor='k')
            x = [0, t[0], t[0], t[0]]
            y = [0, 0, t[1], t[1]]
            z = [0, 0, 0, t[2]]
            self.ax.plot(x,y,z, ls=':', c='m')

        if pose_n is not False:
            xyz = t.ravel()
            self.ax.text(*xyz, pose_n, color='k')
            
        if pose_xyz:
            # Draw camera position text
            # Why did I add chessboard_sq here?
            xyz = t.ravel()# + chessboard_sq
            text = str(tuple(xyz.round(2)))
            self.ax.text(*xyz, text, color='k')



    def draw_orientation(self, r, t=np.array([[0],[0],[0]])):
        
        f = 0.1
        h, w = self.chessboard_dim
        h *= self.chessboard_sq * f
        w *= self.chessboard_sq * f
        '''
        f = 0.002
        w, h = np.array(self.img_dim) * f
        '''
        d = w

        camera_center = t.T[0]
        camera = np.array([[-w,-h,0], [w,-h,0], [w,h,0], [-w,h,0], [0,0,d]])
        camera = np.array([Rotate(i, *-r) for i in camera])

        '''
        base = [camera[0], camera[1], camera[2], camera[3], camera[0]]
        #sides1 = [camera[0], camera[2]]
        #sides2 = [camera[1], camera[3]]
        #points = [base + sides1 + sides2]
        apex1 = [camera[0], camera[4], camera[2]]
        apex2 = [camera[1], camera[4], camera[3]]
        points = [base + apex1 + apex2]
        points += camera_center
        #self.ax.add_collection3d(Line3DCollection(points, colors='k', linewidths=1, alpha=0.5))
        '''
        # Must set alpha separately and/or before fc (bug): https://stackoverflow.com/a/48230450/11581064
        #self.ax.add_collection3d(Poly3DCollection([base+camera_center],
        #                        facecolors='cyan', edgecolors='k', linewidths=1, alpha=0.5))
        '''
        base = [camera[0], camera[1], camera[2], camera[3]]
        side1 = [camera[0], camera[4], camera[1]]
        side2 = [camera[0], camera[4], camera[3]]
        side3 = [camera[2], camera[4], camera[1]]
        side4 = [camera[2], camera[4], camera[3]]
        pyr = [base + side1 + side2 + side3 + side4]
        pyr += camera_center
        '''
        v = camera + camera_center
        pyr = [[v[0],v[1],v[4]], [v[0],v[3],v[4]], [v[2],v[1],v[4]], [v[2],v[3],v[4]], [v[0],v[1],v[2],v[3]]]
        pc = Poly3DCollection(pyr, linewidths=1, alpha=0.25)
        pc.set_facecolor('cyan')
        pc.set_edgecolor('k')
        self.ax.add_collection3d(pc)

    
    def getRt(self, r, t):
        R, R_jacobian = cv.Rodrigues(r)
        t = -np.dot(R.T, t)
        t[2] = -t[2]
        return R, t


    def getCorners(self, im, img_corners, r, t):

        img = im.copy()
        cv.drawChessboardCorners(img, self.chessboard_dim, img_corners, True)
        origin = tuple(img_corners[0].ravel())

        obj_corners_axes = self.n_sq * np.eye(3)
        obj_corners_axes[2] = -obj_corners_axes[2] # z --> -z; unsure why this is necessary...
        img_corners_axes, jacobian = cv.projectPoints(obj_corners_axes, r, t, 
                                                      self.K, self.distortion_coeffs)
        img_corners_axes = img_corners_axes.astype(int)

        lw = 3
        img = cv.arrowedLine(img, origin, tuple(img_corners_axes[0].ravel()), pltBLUE, lw)
        img = cv.arrowedLine(img, origin, tuple(img_corners_axes[1].ravel()), GREEN, lw)
        img = cv.arrowedLine(img, origin, tuple(img_corners_axes[2].ravel()), pltRED, lw)
            
        return img

   

    def getRtCorners0(self):
        self.imgsCorners = []
        #self.Rts = []
        self.R = []
        self.t = []
        #for v,k in self.poses.items():
        for e, (img, p, r, t) in enumerate(zip(self.imgs, self.img_corners, self.rvecs, self.tvecs)):
            corners = self.getCorners(img, p, r, t)
            self.imgsCorners.append(corners)
            R, t = self.getRt(r, t)
            self.extrinsics = np.hstack([R, t])
            self.R.append(R)
            self.t.append(t)

            self.poses[e]['r'] = r
            self.poses[e]['R'] = R
            self.poses[e]['t'] = t
            self.poses[e]['Rt'] = np.hstack([R, t])
            self.poses[e]['corners'] = corners
            
    def getRtCorners(self, rvecs, tvecs):
        for (v,k), c, r, t in zip(self.poses.items(), corners, rvecs, tvecs):
            img = v['img']
            c = v['corners']
            corners = self.getCorners(img, c, r_, t_)

            self.poses[e]['r'] = r
            self.poses[e]['R'] = R
            self.poses[e]['t'] = t
            self.poses[e]['Rt'] = np.hstack([R, t])
    



    def calibrate(self, files):
        # I think these should all go into a dict
        self.files = files

        self.img_corners = []
        obj_corners = []

        self.imgs = []

        self.poses = {}

        for e,f in enumerate(files):
            print(f)

            img = cv.imread(f)
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            #gray = cv.imread(f, 0)

            self.poses[e] = {}
            self.poses[e]['file'] = f
            self.poses[e]['img'] = img
            self.poses[e]['gray'] = gray

            # Find chessboard
            patternWasFound, img_corners = cv.findChessboardCorners(gray, self.chessboard_dim)

            # With corners found, get camera intrinsic and extrinsics
            if patternWasFound:

                # Fine tune corner location to subpixel. Often not necessary
                criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
                img_corners = cv.cornerSubPix(gray, img_corners, (11,11), (-1,-1), criteria)

                self.img_corners.append(img_corners)
                obj_corners.append(self.obj_corners)

                self.poses[e]['corners'] = img_corners


            self.imgs.append(img)

        # Assuming all images are same dimensions...
        self.img_dim = gray.shape[::-1]

        ret = cv.calibrateCamera(obj_corners, self.img_corners, self.img_dim, None, None)
        self.RMS, self.K, self.distortion_coeffs, self.rvecs, self.tvecs = ret
        self.getRtCorners0()

        # Under construction... both getRtCorners and calibrate need to be changed
        #self.RMS, self.K, self.distortion_coeffs, rvecs, tvecs = ret
        #self.getRtCorners(rvecs, tvecs)



    def undistort(self):
        for e,i in enumerate(self.imgs):
            self.poses[e]['undist'] = cv.undistort(i, self.K, self.distortion_coeffs)
        






'''
import re, os
# See GitLab history for two other filepath filtering methods
DIRPATH = '/home/joshua/opencv/samples/data/'
FILES = os.listdir(DIRPATH)
#REGEX = '(left|right)\d+.jpg'
#REGEX = 'left\d+.jpg'
REGEX = 'left07'
FILEPATHS = [os.path.join(DIRPATH, i) for i in FILES if re.match(REGEX, i)]

CHESSBOARD_SQ = 1
CHESSBOARD_DIM = (6, 9)

pose = Poser(CHESSBOARD_DIM, CHESSBOARD_SQ)
pose.calibrate(FILEPATHS)
pose.show()
'''
