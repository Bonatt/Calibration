import numpy as np
import cv2 as cv
import imutils, sys, os, time
from imutils.video import FPS
from scipy import ndimage
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from scipy import stats
from math import sin, cos
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d


sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from helpers import *



#FILE = 'chessboard2d_warped.png'
FILE = '/home/joshua/opencv/samples/data/left{0:02d}.jpg'.format(1)
im = cv.imread(FILE)

# ORB
'''
orb = cv.ORB_create()
kp = orb.detect(im, None)
kp, des = orb.compute(im, kp)

im2 = cv.drawKeypoints(im, kp, GREEN, flags=0)
pltimshow(im2)
'''

### Hough Lines
'''
k = 3
kernelsize = (k, k)
rho = 1
theta = np.pi/180
minLineLength = 0.05 * 960
maxLineGap = 5
threshold = 100#int(minLineLength) + maxLineGap

gray = im.copy()
gray = cv.GaussianBlur(gray, kernelsize, 0)
edges = Canny(gray)
edges = cv.GaussianBlur(edges, kernelsize, 0)
lines = cv.HoughLinesP(edges, rho=rho, theta=theta, threshold=threshold,
                minLineLength=minLineLength, maxLineGap=maxLineGap)
for x1,y1,x2,y2 in lines[:,0]:
   cv.line(gray, (x1, y1), (x2, y2), GREEN, 1)
pltimshow(gray)
'''



chessboard_square_width = 100 # mm
chessboard_dim = (6, 9) # n_corners, m_corners

gray = cv.cvtColor(im, cv.COLOR_BGR2GRAY)
patternWasFound, corners = cv.findChessboardCorners(gray, chessboard_dim)
#corners = np.sort(np.sort(corners, axis=0), axis=1)
# Sorted to get top-left be 0th.
#corners0 = corners.reshape(-1,2)
#corners = corners[np.lexsort((corners0[:,0], corners0[:,1]))]
#criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
#corners2 = cv.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
im_corners = im.copy()
cv.drawChessboardCorners(im_corners, chessboard_dim, corners, patternWasFound)
#pltimshow(im_corners)


# https://docs.opencv.org/4.0.0/dc/dbb/tutorial_py_calibration.html
# https://stackoverflow.com/a/49172755/11581064
#imagePoints = np.array([[i[0] for i in corners]], np.float32) # 2D points in image plane
imagePoints = corners.reshape(1,-1,2).astype(np.float32)
#objectPoints = np.zeros((np.prod(chessboard_dim),3)) # 3D points in real world space
#objectPoints[:,:2] = np.mgrid[:chessboard_dim[0], :chessboard_dim[1]].T.reshape(-1,2)
#objectPoints = np.array([objectPoints])
x = np.tile(range(0,chessboard_dim[0]*chessboard_square_width, chessboard_square_width), chessboard_dim[1])
y = np.repeat(range(0, chessboard_dim[1]*chessboard_square_width, chessboard_square_width), chessboard_dim[0])
z = np.zeros(np.prod(chessboard_dim))
objectPoints = np.vstack([x, y, z]).T
objectPoints = objectPoints.reshape(1,-1,3).astype(np.float32)

ret = cv.calibrateCamera(objectPoints, imagePoints, gray.shape[::-1], None, None)
rms, cameraMatrix, distCoeffs, rvecs, tvecs = ret
rvec, tvec = rvecs[0], tvecs[0]

def draw(img, corners, imgpts):
    corners = corners.astype(int)
    imgpts = imgpts.astype(int)
    # top-left corner (closest to (0,0))
    i = 0#np.linalg.norm(corners, axis=2).argmin() # 0
    corner = tuple(corners[i].ravel())
    img = cv.line(img, corner, tuple(imgpts[0].ravel()), pltBLUE, 2)
    img = cv.line(img, corner, tuple(imgpts[1].ravel()), GREEN, 2)
    img = cv.line(img, corner, tuple(imgpts[2].ravel()), pltRED, 2)
    return img
#criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
#corners2 = cv.cornerSubPix(gray, corners,(11,11),(-1,-1),criteria)
axis = min(chessboard_dim)*np.eye(3)*chessboard_square_width
axis = axis*[[1],[1],[-1]] # z --> -z
imgpts, jac = cv.projectPoints(axis, rvec, tvec, cameraMatrix, distCoeffs) 
im_axes = draw(im_corners, corners, imgpts)
pltimshow(im_axes)


#cameraMatrix_new, roi = cv.getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, gray.shape[::-1],1,gray.shape[::-1])
##x, y, w, h = roi
#im2 = cv.undistort(im, cameraMatrix, distCoeffs)#, None, None)#cameraMatrix_new)
#pltimshow(np.hstack([im, im2]))
cameraMatrix, roi = cv.getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, gray.shape[::-1], 0)
#x, y, w, h = roi
im0 = im.copy()
im = cv.undistort(im, cameraMatrix, distCoeffs)#, None, None)#cameraMatrix_new)
pltimshow(np.hstack([im0, im]))


R, R_jacobian = cv.Rodrigues(rvec)

# np.matrix just changes dtype from array to matrix... Doesn't work otherwise, though
#cameraPosition = -np.matrix(R).T * np.matrix(tvec)
cameraPosition = np.dot(-R.T, tvec)
cameraPosition[2] = -cameraPosition[2]


fig = plt.figure()
ax = plt.axes(projection='3d')
#s = 100*chessboard_square_width/np.linalg.norm(cameraPosition)
s = 100*np.prod(np.array(chessboard_dim)+1)/np.linalg.norm(cameraPosition)
#ax.scatter3D(*(0,0,0), marker='s', facecolors='k', s=s)
ax.scatter3D(*cameraPosition, facecolors='none', edgecolor='k')
#a, b = cameraPosition.min(), cameraPosition.max()
#ax.set_xticks([a, 0, b])
#ax.set_yticks([a, 0, b])
#ax.set_zticks([a, 0, b])
#ax.set_aspect('equal')
#plt.tight_layout()

#w, h = np.array(chessboard_dim)/2 * chessboard_square_width
#x = [-w, -w, w, w, -w]
#y = [-h, h, h, -h, -h]
w, h = np.array(chessboard_dim) * chessboard_square_width
x = [0, 0, w, w, 0]
y = [0, h, h, 0, 0]
z = [0, 0, 0, 0, 0]
ax.plot(x,y,z, ls='-', c='k')

x = [0, cameraPosition[0], cameraPosition[0], cameraPosition[0]]
y = [0, 0, cameraPosition[1], cameraPosition[1]]
z = [0, 0, 0, cameraPosition[2]]
ax.plot(x,y,z, ls=':', c='m')

plt.title(str(tuple(i[0] for i in cameraPosition)))
lim0 = 1.1*max(cameraPosition.min(), cameraPosition.max())
lim = (-lim0, lim0)
ax.set_xlim(lim0, -lim0)
ax.set_ylim(lim)
ax.set_zlim(0, 2*lim0)
ax.set_xlabel('X (au)')
ax.set_ylabel('Y (au)')
ax.set_zlabel('Z (au)')
ax.xaxis.label.set_color('blue')
ax.yaxis.label.set_color('green')
ax.zaxis.label.set_color('red')
ax.tick_params(axis='x', colors='blue')
ax.tick_params(axis='y', colors='green')
ax.tick_params(axis='z', colors='red')
plt.show(block=False)







#Rt = np.hstack([R, tvec])
#P = cameraMatrix * Rt
# R.T * tvec

#np.vstack([np.hstack([R, tvec]), [0,0,0,1]])


'''
R2 = R.T
tvec2 = -R2 * tvec
T = np.eye(4,4)
T[:3, :3] = R2
T[:3, 3:4] = tvec2
'''

#axis = np.eye(3)*3
#theta = np.linalg.norm(rvec)
#theta*180/np.pi




'''
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(*tvec)
ax.scatter3D(*[0,0,0])
plt.tight_layout()
plt.show()
'''









F = 1 # Focal length in world units, typically expressed in millimeters.
px, py = 1, 1 # Size of the pixel in world units.
fx, fy = F/px, F/py # Focal length in pixels. 
cx, cy = 0, 0 # Optical center (the principal point), in pixels.
s = 0 # Skew coefficient, which is non-zero if the image axes are not perpendicular.
# s = fx * math.tan(alpha) # alpha = ??

#K = [[F,0,px], [0,F,py], [0,0,1]] # Intrinsics
#K = [[fx,0,0], [s,fy,0], [cx,cy,1]] # Intrinsics
K = [[fx,s,cx], [s,fy,cy], [0,0,1]] # Intrinsic camera parameters
#B = [[r11,r12,r13,t1],[r21,r22,r23,t2],[r31,r32,r33,t3]] # Extrinsic camera parameters
#H = K*B


def translate(a, tx=0, ty=0, d=2):
    if d==2:
        t = [[1,0,tx], [0,1,ty], [0,0,1]]
    return t*a.T

def scale(a, sx=1, sy=1, d=2):
    if d==2:
        s = [[sx,0,0], [0,sy,0], [0,0,1]]
    return s*a.T

def rotate(a, r=0, d=2):
    if d==2:
        r = [[cos(r), -sin(r), 0], [sin(r), cos(r), 0], [0,0,1]]
    return r*a.T

def shear(a, bx=0, by=0, d=2):
    if d==2:
        b = [[1,bx,0], [by,1,0], [0,0,1]]
    return a*b.T

def composite_example(a, tx=0, ty=0, sx=1, sy=1, r=0):
    a = scale(a)
    a = rotate(a)
    a = translate(a)
    return a






