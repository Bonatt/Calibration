import numpy as np
import cv2 as cv
import imutils, sys, os, time
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.patches import PathPatch, Rectangle
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import math, re

import sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from Helpers.helpers import *



def CalibrateCamera(imgs, chessboard_dim=(6,9), chessboard_sq=1,
    return_img=False, return_img_corners=False, return_RT=False):
    '''Input list of filepaths imgs, return camera matrix and camera pose per img'''

    def getCorners(im, corners, cameraMatrix, distanceCoeffs, rvec, tvec, chessboard_dim, chessboard_sq):

        im_corners = im.copy()
        cv.drawChessboardCorners(im_corners, chessboard_dim, corners, True)

        def show_axes(im, cameraMatrix, distCoeffs, rvec, tvec, chessboard_sq):
            #n_sq = min(chessboard_dim)*chessboard_sq - 1 # length of axes to draw
            n_sq = 3 * chessboard_sq - 1
            axes = n_sq * np.eye(3)
            axes[2] = -axes[2] # z --> -z; unsure why this is necessary...
            imagePoints_axes, jacobian = cv.projectPoints(axes, rvec, tvec, cameraMatrix, distCoeffs)

            def draw_axes(im, corners, imagePoints):
                corners = corners.astype(int)
                imagePoints = imagePoints.astype(int)
                corner = tuple(corners[0].ravel())
                lw = 3
                im = cv.arrowedLine(im, corner, tuple(imagePoints[0].ravel()), pltBLUE, lw)
                im = cv.arrowedLine(im, corner, tuple(imagePoints[1].ravel()), GREEN, lw)
                im = cv.arrowedLine(im, corner, tuple(imagePoints[2].ravel()), pltRED, lw)
                return im

            return draw_axes(im, imagePoints[0], imagePoints_axes)

        im_axes = show_axes(im_corners, cameraMatrix, distanceCoeffs, rvec, tvec, chessboard_sq)

        return im_axes

    def getRT(rvec, tvec):
        R, R_jacobian = cv.Rodrigues(rvec)
        T = -np.dot(R.T, tvec)
        T[2] = -T[2]
        return R, T

    def setObjectPoints(chessboard_dim, chessboard_sq):
        w, h = chessboard_dim
        x = np.tile(range(0, w*chessboard_sq, chessboard_sq), h)
        y = np.repeat(range(0, h*chessboard_sq, chessboard_sq), w)
        z = np.zeros(w*h)
        objectPoints = np.vstack([x, y, z]).T
        objectPoints = objectPoints.reshape(1,-1,3).astype(np.float32)
        return objectPoints

    objectPoints_ = setObjectPoints(chessboard_dim, chessboard_sq)

    imagePoints = []
    objectPoints = []

    ims_corners = []
    ims = []

    for img in imgs:
        print(img)
    
        im = cv.imread(img)
        gray = cv.cvtColor(im, cv.COLOR_BGR2GRAY)
        #gray = cv.imread(img, 0)

        # Find chessboard
        patternWasFound, corners = cv.findChessboardCorners(gray, chessboard_dim)

        # With corners found, get camera intrinsics and extrinsics
        if patternWasFound:
            
            # Fine tune corner location to subpixel. Often not necessary
            criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
            corners = cv.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)

            imagePoints.append(corners)
            objectPoints.append(objectPoints_)

            #if return_img_corners:
            #    ims_corners.append(getCorners(im, corners, cameraMatrix, distanceCoeffs, rvec, tvec, 
            #                                  chessboard_dim, chessboard_sq))

        #if return_img:
        ims.append(im)

    # Assuming all images are same dimensions...
    imageDims = gray.shape[::-1]

    ret = cv.calibrateCamera(objectPoints, imagePoints, imageDims, None, None)
    rms, cameraMatrix, distanceCoeffs, rvecs, tvecs = ret

    if return_img_corners:
        for im, rvec, tvec in zip(ims, rvecs, tvecs):
            ims_corners.append(getCorners(im, corners, cameraMatrix, distanceCoeffs, rvec, tvec,
                                          chessboard_dim, chessboard_sq))

    if return_RT:
        RTs = [getRT(rvec, tvec) for rvec, tvec in zip(rvecs, tvecs)]

    # This stack is ugly and doesn't work. It's all or nothing currently, breaking.
    if return_img and return_img_corners and return_RT:
        return objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, \
               ims, ims_corners, RTs
    elif return_img and return_img_corners and not return_RT:
        return objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, \
               ims, ims_corners
    elif return_img and not return_img_corners and return_RT:
        return objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, \
               ims, RTs
    elif return_img and not return_img_corners and not return_RT:
        return objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, \
               ims
    else:
        return objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs




'''
# See GitLab history for two other filepath filtering methods
DIRPATH = '/home/joshua/opencv/samples/data/'
FILES = os.listdir(DIRPATH)
REGEX = '(left|right)\d+.jpg'
FILEPATHS = [os.path.join(DIRPATH, i) for i in FILES if re.match(REGEX, i)]

CHESSBOARD_SQ = 1
CHESSBOARD_DIM = (6, 9)

ret = CalibrateCamera(FILEPATHS, CHESSBOARD_DIM, CHESSBOARD_SQ)
objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs = ret

#im_undistorted = cv.undistort(im, cameraMatrix, distCoeffs)
'''


    
