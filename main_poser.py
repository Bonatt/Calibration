import sys, os, re
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from Helpers.helpers import *

from poser import Poser

# See GitLab history for two other filepath filtering methods
DIRPATH = '/home/joshua/opencv/samples/data/'
FILES = os.listdir(DIRPATH)
#REGEX = '(left|right)\d+.jpg'
REGEX = 'left\d+.jpg'
#REGEX = 'left07'
# pose if left02 is incorrect. Why?
FILEPATHS = [os.path.join(DIRPATH, i) for i in FILES if re.match(REGEX, i)]

CHESSBOARD_SQ = 1
CHESSBOARD_DIM = (6, 9)


poser = Poser(CHESSBOARD_DIM, CHESSBOARD_SQ)
poser.calibrate(FILEPATHS)
poser.undistort()

# Show all
poser.show(pose_n=True, pose_xyz=False, pose_lines=False)
plt.savefig('Results/poses.png')

# Show one
i = 0
img = poser.poses[i]
f = re.search('^.*/(.*)\..*$', img['file']).group(1)
poser.show([i], pose_n=True, pose_xyz=True, pose_lines=True)
plt.savefig('Results/{}_pose.png'.format(f))
pltimshow(img['corners'], save=True, savename='Results/{}_detected.png'.format(f))
stack = np.hstack([img['img'], img['undist']])
pltimshow(stack, save=True, savename='Results/{}_undistorted.png'.format(f))




### Reprojection error, ~0.5 pixels
mean_error = 0
for c, r, t in zip(poser.img_corners, poser.rvecs, poser.tvecs):
    img_corners, _ = cv.projectPoints(poser.obj_corners, r, t, poser.K, poser.distortion_coeffs)
    mean_error += cv.norm(c, img_corners, cv.NORM_L2)/len(img_corners)
print('total error: ', mean_error/len(poser.obj_corners))
