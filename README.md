# Camera Calibration and Camera Egopose.

Given an image containing a visual calibration tool, e.g., chessboard of known dimensions, calibration of the camera, i.e., estimation of the intrinsic (focal length, skew, distortion, image center) and/or extrinsic (position, orientation) parameters of the camera is possible.

*Short introduction to camera calibration here.*

OpenCV has 28 sample calibration images in [/samples/data/](https://github.com/opencv/opencv/tree/master/samples/data) as left*.jpg and right*.jpg. Below is an example left07.jpg:

![](Data/left01.jpg)

The corners of this chessboard are of interest. Other calibration patterns exist, such as dot and deltille grids, but rectangular chessboard patterns are standard. OpenCV has a native function [findChessboardCorners](https://github.com/opencv/opencv/blob/master/modules/calib3d/src/chessboard.cpp); this was employed here. 
To summarize, this function takes a greyscale image, determines if a chessboard of input dimensions is present, and if so, finds and returns the internal corners of the chessboard. Below is the same image with the chessboard detected, where arrows have been added to indicate world origin and axes (red z, green y, blue x):

![](Results/left01_detected.png)

*Short description of calibrateCambera() outputs here.*

From this calibration image (and preferably many other heterogenous images), the camera may be calibrted
and poses estimated. The calibration allows for images to be undistorted, e.g., origin left, undistorted right:

![](Results/left01_undistorted.png)

The intrinsitc 3x3 camera matrix `K` for the employed camera is

```
K = [[536.07343026,   0.        , 342.3703876 ],
     [  0.        , 536.01634483, 235.53685658],
     [  0.        ,   0.        ,   1.        ]]
```

The extrinsic 3x3 rotation matrix `R` and 3x1 translation vector `t` for left01.jpg are

```
R = [-0.00980078,  0.96222053,  0.27209483],
    [-0.98583135,  0.03626967, -0.16377136],
    [-0.16745296, -0.2698447 ,  0.94823169]

t = [[ 3.35271598],
     [ 7.37106399],
     [15.05926559]]
```

Camera pose(s) wrt the chessboard may also be visualized. Shown below are the estimated poses 
for all left*.jpg images, with left01.jpg labelled "0" and also showed alone. All distances are a function 
of the width of one chessboard square, set here to 1 au.

![](Results/poses.png)

![](Results/left01_pose.png)



The L2 norm between the detected chessboard corners and expected chessboard corners given estimated 
intrinsic and extrinsic parameters is the reprojection error. The mean reprojection error for the analysis
of this series of images is `0.53` pixels. Because this error is less than one, the error is subpixel and 
therefore may be considered to be significantly -- if not only -- caused by pixel discretization. 
A greater resolution image may alleviate this source of uncertainty.
