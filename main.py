import numpy as np
import cv2 as cv
import imutils, sys, os, time
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.patches import PathPatch, Rectangle
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import math, re

sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from Helpers.helpers import *

from camera_calibrate import CalibrateCamera
from camera_pose import *


# See GitLab history for two other filepath filtering methods
DIRPATH = '/home/joshua/opencv/samples/data/'
FILES = os.listdir(DIRPATH)
#REGEX = '(left|right)\d+.jpg'
#REGEX = 'left\d+.jpg'
REGEX = 'left07'
FILEPATHS = [os.path.join(DIRPATH, i) for i in FILES if re.match(REGEX, i)]

CHESSBOARD_SQ = 1
CHESSBOARD_DIM = (6, 9)

ret = CalibrateCamera(FILEPATHS, CHESSBOARD_DIM, CHESSBOARD_SQ,
                      return_img=True, return_img_corners=True, return_RT=True)
objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, imgs, imgs_corners, RTs = ret





fig = plt.figure(figsize=(10,10))
ax = plt.axes(projection='3d')
for cameraOrientation, (R, cameraPosition) in zip(rvecs, RTs):
    plot_pose(cameraPosition, cameraOrientation, ax=ax)
#for cameraOrientation, cameraPosition in zip(rvecs, tvecs):
#    plot_pose(cameraPosition, cameraOrientation, ax=ax)
plt.show(block=False)


print(rvecs[0])
print(tvecs[0])
print(RTs[0][1])
pltimshow(imgs_corners[0])
