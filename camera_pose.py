import numpy as np
import cv2 as cv
import imutils, sys, os, time
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.patches import PathPatch, Rectangle
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import math, re

sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/')
from Helpers.helpers import *

from camera_calibrate import CalibrateCamera


"""
# See GitLab history for two other filepath filtering methods
DIRPATH = '/home/joshua/opencv/samples/data/'
FILES = os.listdir(DIRPATH)
REGEX = '(left|right)\d+.jpg'
FILEPATHS = [os.path.join(DIRPATH, i) for i in FILES if re.match(REGEX, i)]

CHESSBOARD_SQ = 1
CHESSBOARD_DIM = (6, 9)

ret = CalibrateCamera(FILEPATHS, CHESSBOARD_DIM, CHESSBOARD_SQ, 
                      return_img=True, return_img_corners=True, return_RT=True)
objectPoints, imagePoints, rms, cameraMatrix, distanceCoeffs, rvecs, tvecs, imgs, imgs_corners, RTs = ret



# To map the above to the below. Temporary for a few days or whatever
i = 0
P = FILEPATHS[i].split('/')[-1]
rvec, tvec = rvecs[i], tvecs[i]
im_corners = imgs_corners[i]
R, cameraPosition = RTs[i]
cameraOrientation = rvec
chessboard_sq, chessboard_dim = CHESSBOARD_SQ, CHESSBOARD_DIM
"""








def show_axes(im, cameraMatrix, distCoeffs, rvec, tvec, chessboard_sq=1):
    #n_sq = min(chessboard_dim)*chessboard_sq - 1 # length of axes to draw
    n_sq = 3 * chessboard_sq - 1
    axes = n_sq * np.eye(3)
    axes[2] = -axes[2] # z --> -z; unsure why this is necessary...
    imagePoints_axes, imagePoints_axes_jacobian = cv.projectPoints(axes, rvec, tvec, cameraMatrix, distCoeffs)

    def draw_axes(im, corners, imagePoints):
        corners = corners.astype(int)
        imagePoints = imagePoints.astype(int)
        corner = tuple(corners[0].ravel())
        lw = 3
        im = cv.arrowedLine(im, corner, tuple(imagePoints[0].ravel()), pltBLUE, lw)
        im = cv.arrowedLine(im, corner, tuple(imagePoints[1].ravel()), GREEN, lw)
        im = cv.arrowedLine(im, corner, tuple(imagePoints[2].ravel()), pltRED, lw)
        return im

    return draw_axes(im, imagePoints[0], imagePoints_axes)

#im_axes = show_axes(im_corners, cameraMatrix, distanceCoeffs, rvec, tvec, chessboard_sq=1)
#pltimshow(im_axes)#, save=True, savename='Results/{}_detected.png'.format(P))



# Draw chessboard position as origin
def draw_chessboard(ax, chessboard_dim=(6,9), chessboard_sq=1):
    '''Draw chessboard at origin'''
    c = 0
    z = [0,0,0,0]
    w, h = np.array(chessboard_dim)
    for i in range(-1, w):
        for j in range(-1, h):
            x = np.array([i, i, i+1, i+1]) * chessboard_sq
            y = np.array([j, j+1, j+1, j]) * chessboard_sq
            verts = [list(zip(x,y,z))]
            fc = 'w' if c%2 else 'k'
            ax.add_collection3d(Poly3DCollection([list(zip(x,y,z))], facecolor=fc, alpha=0.5))
            c += 1
        if h%2: c += 1

    # Draw chessboard axes
    n_sq = 3 * chessboard_sq - 1
    xcolor, ycolor, zcolor = 'blue', 'green', 'red'
    ax.quiver(0,0,0, n_sq,0,0, color=xcolor, lw=2)
    ax.quiver(0,0,0, 0,n_sq,0, color=ycolor, lw=2)
    ax.quiver(0,0,0, 0,0,n_sq, color=zcolor, lw=2)


def Rotate(a, theta=0, phi=0, psi=0):
    '''a of form [x,y,z]; (x,y,z) --> (theta, phi, psi)'''

    def s(x):
        return math.sin(x)
    def c(x):
        return math.cos(x)

    Rx = np.array([[1, 0, 0], [0, c(theta), s(theta)], [0, -s(theta), c(theta)]])
    Ry = np.array([[c(phi), 0, -s(phi)], [0, 1, 0], [s(phi), 0, c(phi)]])
    Rz = np.array([[c(psi), -s(psi), 0], [s(psi), c(psi), 0], [0, 0, 1]])

    return Rz.dot(Ry.dot(Rx.dot(a)))



def plot_pose(cameraPosition, cameraOrientation, chessboard_dim=(6,9), chessboard_sq=1, ax=None, **kwargs):

    ### Draw camera pose
    if not ax:
        fig = plt.figure(figsize=(10,10))
        ax = plt.axes(projection='3d')
   
    # Draw chessboard position as origin
    def draw_chessboard(ax, chessboard_dim=(6,9), chessboard_sq=1):
        '''Draw chessboard at origin'''
        c = 0
        z = [0,0,0,0]
        w, h = np.array(chessboard_dim)
        for i in range(-1, w):
            for j in range(-1, h):
                x = np.array([i, i, i+1, i+1]) * chessboard_sq
                y = np.array([j, j+1, j+1, j]) * chessboard_sq
                verts = [list(zip(x,y,z))]
                fc = 'w' if c%2 else 'k'
                ax.add_collection3d(Poly3DCollection([list(zip(x,y,z))], facecolor=fc, alpha=0.5))
                c += 1
            if h%2: c += 1     

        # Draw chessboard axes
        global n_sq, xcolor, ycolor, zcolor
        n_sq = 3 * chessboard_sq - 1
        xcolor, ycolor, zcolor = 'blue', 'green', 'red'
        ax.quiver(0,0,0, n_sq,0,0, color=xcolor, lw=2)
        ax.quiver(0,0,0, 0,n_sq,0, color=ycolor, lw=2)
        ax.quiver(0,0,0, 0,0,n_sq, color=zcolor, lw=2)


    global draw_position
    def draw_position(ax, cameraPosition):
        # Draw camera position, line
        ax.plot(*cameraPosition, 'x', c='m')#facecolors='none', edgecolor='k')
        x = [0, cameraPosition[0], cameraPosition[0], cameraPosition[0]]
        y = [0, 0, cameraPosition[1], cameraPosition[1]]
        z = [0, 0, 0, cameraPosition[2]]
        ax.plot(x,y,z, ls=':', c='m')
    
        # Draw camera position text
        # Why did I add chessboard_sq here?
        pos = cameraPosition.ravel()# + chessboard_sq
        pos_text = str(tuple(pos.round(2)))
        ax.text(*pos, pos_text, color='m')
    
    global draw_orientation
    def draw_orientation(ax, cameraOrientation, chessboard_dim, chessboard_sq, 
                         cameraPosition=np.array([[0],[0],[0]])):
        c = 0.2
        ch, cw = chessboard_dim
        ch *= chessboard_sq * c
        cw *= chessboard_sq * c

        global camera_center
        camera_center = cameraPosition.T[0]

        global camera
        camera = np.array([[-cw,-ch,0], [cw,-ch,0], [cw,ch,0], [-cw,ch,0]])#, [cw,ch,h]])
        camera = np.array([Rotate(i, *-cameraOrientation) for i in camera])

        global points
        points_base = [camera[0], camera[1], camera[2], camera[3], camera[0]]
        points_sides1 = [camera[0], camera[2]]
        points_sides2 = [camera[1], camera[3]]
        points = [points_base + points_sides1 + points_sides2]
        points += camera_center
        ax.add_collection3d(Line3DCollection(points, colors='m', linewidths=1))
        #ax.add_collection3d(Poly3DCollection(points, facecolors='cyan', linewidths=1, edgecolors='r', alpha=.25))

        '''
        p = camera_center
        d = 2 * R.T * [1, 1, -1]
        ax.quiver(*p, *d[:,0], color=xcolor, lw=2)
        ax.quiver(*p, *d[:,1], color=ycolor, lw=2)
        ax.quiver(*p, *d[:,2], color=zcolor, lw=2)
        ''' 

    draw_chessboard(ax, chessboard_dim, chessboard_sq)
    draw_position(ax, cameraPosition)
    draw_orientation(ax, cameraOrientation, chessboard_dim, chessboard_sq, cameraPosition)

    # Set axes limits
    lim = abs(cameraPosition).max()
    #lim = sorted(cameraPosition)[1][0]
    ax.set_xlim(lim, -lim)
    ax.set_ylim(-lim, lim)
    ax.set_zlim(0, 2*lim)

    # Label, color axes
    #plt.title(FILE)
    ax.set_xlabel('X (au)'); ax.set_ylabel('Y (au)'); ax.set_zlabel('Z (au)')
    ax.xaxis.label.set_color(xcolor); ax.tick_params(axis='x', colors=xcolor)
    ax.yaxis.label.set_color(ycolor); ax.tick_params(axis='y', colors=ycolor)
    ax.zaxis.label.set_color(zcolor); ax.tick_params(axis='z', colors=zcolor)

    # Set background from grey to white
    bgcolor = (1,1,1)
    ax.w_xaxis.set_pane_color(bgcolor)
    ax.w_yaxis.set_pane_color(bgcolor)
    ax.w_zaxis.set_pane_color(bgcolor)

    # View angle based on calibration
    '''
    # atan2 logic from ClockReader
    r = np.linalg.norm(cameraPosition)
    theta_deg = math.acos(cameraPosition[2]/r)*180/math.pi
    phi_rad = math.atan(cameraPosition[1]/cameraPosition[0])
    phi_deg = phi_rad * 180/math.pi
    phi_rad2 = math.atan(cameraPosition[1]/cameraPosition[0])
    arctans_are_same = np.sign(phi_rad) == np.sign(phi_rad2)
    rotate180 = arctans_are_same * 180
    phi_deg = (phi_deg + rotate180)%360
    ax.view_init(azim=phi_deg-90)
    '''
    #ax.dist *= 0.9
    '''
    global angles
    angles = cameraOrientation.ravel() * 180/math.pi + [0,0,90] #y, z
    ax.view_init(elev=angles[1], azim=angles[2])
    '''
 
    plt.tight_layout()
    #plt.show(block=False)
    #plt.savefig('Results/{}_pose.png'.format(P))

    # left09
    # ax.elev = 121.43205719566095
    # ax.azim = 15.668874425299748
        
    #return fig, ax


#pose_fig, pose_ax = plot_pose(cameraPosition, cameraOrientation, chessboard_dim, chessboard_sq)














def reshow(fig, show=True):
    '''https://stackoverflow.com/a/31731945/11581064
    Create a dummy figure and use its manager to display "fig".
    Used to reshow fig.'''
    dummy = plt.figure()
    new_manager = dummy.canvas.manager
    new_manager.canvas.figure = fig
    fig.set_canvas(new_manager.canvas)
    if show:
        plt.show()

'''
fig = plt.figure()
plt.subplot(121)
plt.imshow(im_axes)
plt.subplot(122)
plt.imshow(posePlot)
plt.show()
'''




